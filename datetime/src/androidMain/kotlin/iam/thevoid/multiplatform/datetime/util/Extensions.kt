package iam.thevoid.multiplatform.datetime.util

import android.content.Context
import iam.thevoid.multiplatform.datetime.R
import iam.thevoid.multiplatform.datetime.model.DayOfWeek

fun DayOfWeek.shortName(context: Context) = when (this) {
    DayOfWeek.MONDAY -> context.getString(R.string.day_of_week_short_mo)
    DayOfWeek.TUESDAY -> context.getString(R.string.day_of_week_short_tu)
    DayOfWeek.WEDNESDAY -> context.getString(R.string.day_of_week_short_we)
    DayOfWeek.THURSDAY -> context.getString(R.string.day_of_week_short_th)
    DayOfWeek.FRIDAY -> context.getString(R.string.day_of_week_short_fr)
    DayOfWeek.SATURDAY -> context.getString(R.string.day_of_week_short_sa)
    DayOfWeek.SUNDAY -> context.getString(R.string.day_of_week_short_su)
}

fun DayOfWeek.name(context: Context) = when (this) {
    DayOfWeek.MONDAY -> context.getString(R.string.day_of_week_mo)
    DayOfWeek.TUESDAY -> context.getString(R.string.day_of_week_tu)
    DayOfWeek.WEDNESDAY -> context.getString(R.string.day_of_week_we)
    DayOfWeek.THURSDAY -> context.getString(R.string.day_of_week_th)
    DayOfWeek.FRIDAY -> context.getString(R.string.day_of_week_fr)
    DayOfWeek.SATURDAY -> context.getString(R.string.day_of_week_sa)
    DayOfWeek.SUNDAY -> context.getString(R.string.day_of_week_su)
}