package iam.thevoid.multiplatform.datetime.util

import iam.thevoid.multiplatform.datetime.model.Date
import iam.thevoid.multiplatform.datetime.model.DateTime
import iam.thevoid.multiplatform.datetime.model.DayOfWeek
import iam.thevoid.multiplatform.datetime.model.Month
import iam.thevoid.multiplatform.datetime.model.Time
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import kotlin.time.Duration
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.milliseconds

actual object DateUtils {
    actual fun createDate(): Date {
        return Calendar.getInstance().createDate()
    }

    actual fun toPlatformDate(date: Date): PlatformDate {
        return date.calendar().time
    }
    actual fun toPlatformDate(date: DateTime): PlatformDate {
        return date.calendar().time
    }

    actual fun platformDateToDateTime(date: PlatformDate): DateTime {
        return Calendar.getInstance().apply { time = date }.createDateTime()
    }

    actual fun platformDateToDate(date: PlatformDate): Date {
        return Calendar.getInstance().apply { time = date }.createDate()
    }

    actual fun platformDateToTime(date: PlatformDate): Time {
        return Calendar.getInstance().apply { time = date }.createTime()
    }

    actual fun createTime(): Time {
        return Calendar.getInstance().createTime()
    }

    actual fun between(start: DateTime, end: DateTime): Duration {
        val calStart = minOf(start, end).calendar()
        val calEnd = maxOf(start, end).calendar()
        return (calEnd.timeInMillis - calStart.timeInMillis).milliseconds
    }

    actual fun between(start: Date, end: Date): Duration {
        val calStart = minOf(start, end).calendar()
        val calEnd = maxOf(start, end).calendar()
        return (calEnd.timeInMillis - calStart.timeInMillis).milliseconds
    }

    actual fun overlappedDays(start: DateTime, end: DateTime): Int {
        return overlappedDays(start.date, end.date)
    }

    actual fun overlappedDays(start: Date, end: Date): Int {
        val calStart = minOf(start, end).calendar()
        val calEnd = maxOf(start, end).calendar()

        val dayStart = calStart.indexOfDayFromEpoch()
        val dayEnd = calEnd.indexOfDayFromEpoch()
        return dayEnd - dayStart + 1
    }

    // Start index is 1
    private fun Calendar.indexOfDayFromEpoch(): Int {
        val millisInDay = 1.days.inWholeMilliseconds
        val wholeDays = timeInMillis / millisInDay
        return wholeDays.toInt() + 1
    }

    internal actual fun plus(date: Date, day: Int,): Date {
        return date.calendar().apply {
            add(Calendar.DAY_OF_YEAR, day)
        }.createDate()
    }
    internal actual fun plus(dateTime: DateTime, day: Int, second: Int): DateTime {
        return dateTime.calendar().apply {
            add(Calendar.SECOND, second)
            add(Calendar.DAY_OF_YEAR, day)
        }.createDateTime()
    }

    actual fun parseDateTime(format: String, date: String): DateTime {
        return Calendar.getInstance().apply { time = platformDate(format, date) }.let { calendar ->
            DateTime(calendar.createDate(), calendar.createTime())
        }
    }

    actual fun parseDate(format: String, date: String): Date {
        return parseDateTime(format, date).date
    }

    actual fun parseTime(format: String, date: String): Time {
        return parseDateTime(format, date).time
    }

    actual fun formatTime(format: String, time: Time): String {
        return formatPlatformDate(format, time.calendar().time)
    }

    actual fun formatDate(format: String, date: Date): String {
        return formatPlatformDate(format, date.calendar().time)
    }

    actual fun formatDateTime(format: String, dateTime: DateTime): String {
        return formatPlatformDate(format, dateTime.calendar().time)
    }

    actual fun isToday(dateTime: DateTime): Boolean {
        return isToday(dateTime.date)
    }

    actual fun isToday(date: Date): Boolean {
        val now = createDate()
        return now == date
    }

    @Suppress("MagicNumber") /* CODE TAKEN FROM JAVA SOURCES */
    actual fun isLeapYear(year: Int): Boolean {
        return year and 3 == 0 && (year % 100 != 0 || year % 400 == 0)
    }

    actual fun isLeapYear(date: Date): Boolean {
        return isLeapYear(date.year)
    }

    actual fun isLastDay(dateTime: DateTime): Boolean {
        val now = Calendar.getInstance()
        val checking = dateTime.calendar()
        return now.apply { add(Calendar.DAY_OF_YEAR, -1) } <= checking
    }
    actual fun isThisWeek(dateTime: DateTime): Boolean {
        val now = Calendar.getInstance()
        val checking = dateTime.calendar()
        return now.get(Calendar.WEEK_OF_YEAR) == checking.get(Calendar.WEEK_OF_YEAR)
    }
    actual fun isLastWeek(dateTime: DateTime): Boolean {
        val now = Calendar.getInstance()
        val checking = dateTime.calendar()
        return now.get(Calendar.YEAR) == checking.get(Calendar.YEAR) &&
            now.get(Calendar.WEEK_OF_YEAR) == checking.get(Calendar.WEEK_OF_YEAR)
    }
    actual fun isThisMonth(dateTime: DateTime): Boolean {
        val now = createDate()
        return now.month == dateTime.date.month && now.year == dateTime.date.year
    }
    actual fun isThisYear(dateTime: DateTime): Boolean {
        val now = createDate()
        return now.year == dateTime.date.year
    }

    actual fun dayOfWeek(date: Date): DayOfWeek {
        val calendar = date.calendar()
        return when (calendar.get(Calendar.DAY_OF_WEEK)) {
            Calendar.MONDAY -> DayOfWeek.MONDAY
            Calendar.TUESDAY -> DayOfWeek.TUESDAY
            Calendar.WEDNESDAY -> DayOfWeek.WEDNESDAY
            Calendar.THURSDAY -> DayOfWeek.THURSDAY
            Calendar.FRIDAY -> DayOfWeek.FRIDAY
            Calendar.SATURDAY -> DayOfWeek.SATURDAY
            Calendar.SUNDAY -> DayOfWeek.SUNDAY

            // IMPOSSIBLE, BUT WHO KNOWS
            else -> DayOfWeek.SUNDAY
        }
    }

    actual fun isLastYear(dateTime: DateTime): Boolean {
        val now = Calendar.getInstance()
        val checking = dateTime.calendar()
        return now.apply { add(Calendar.YEAR, -1) } <= checking
    }

    private fun Calendar.createDateTime(): DateTime {
        return DateTime(createDate(), createTime())
    }

    private fun Calendar.createTime(): Time {
        return Time(
            get(Calendar.HOUR_OF_DAY),
            get(Calendar.MINUTE),
            get(Calendar.SECOND),
        )
    }

    private fun Calendar.createDate(): Date {
        return Date(
            get(Calendar.DAY_OF_MONTH),
            get(Calendar.MONTH).let(Month.entries::get),
            get(Calendar.YEAR)
        )
    }
    private fun Date.calendar(): Calendar {
        return Calendar.getInstance().apply {
            timeInMillis = 0
            set(Calendar.DAY_OF_MONTH, this@calendar.day)
            set(Calendar.MONTH, this@calendar.month.ordinal)
            set(Calendar.YEAR, this@calendar.year)
        }
    }
    private fun Time.calendar(): Calendar {
        return Calendar.getInstance().apply {
            timeInMillis = 0
            set(Calendar.SECOND, this@calendar.seconds)
            set(Calendar.MINUTE, this@calendar.minutes)
            set(Calendar.HOUR_OF_DAY, this@calendar.hours)
        }
    }
    private fun DateTime.calendar(): Calendar {
        return Calendar.getInstance().apply {
            timeInMillis = 0
            set(Calendar.DAY_OF_MONTH, this@calendar.date.day)
            set(Calendar.MONTH, this@calendar.date.month.ordinal)
            set(Calendar.YEAR, this@calendar.date.year)
            set(Calendar.SECOND, this@calendar.time.seconds)
            set(Calendar.MINUTE, this@calendar.time.minutes)
            set(Calendar.HOUR_OF_DAY, this@calendar.time.hours)
        }
    }

    private fun platformDate(format: String, date: String): java.util.Date {
        return SimpleDateFormat(format, Locale.getDefault()).parse(date)!!
    }

    private fun formatPlatformDate(format: String, date: java.util.Date): String {
        return SimpleDateFormat(format, Locale.getDefault()).format(date)
    }
}
