package iam.thevoid.multiplatform.datetime.util

actual typealias PlatformDate = java.util.Date

actual val PlatformDate.timeInMillis: Long
    get() = (this as java.util.Date).time
