package iam.thevoid.multiplatform.datetime.model

import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class DateTest {
    @Test
    fun checkDateInRangeEndInclusive() {
        val date = Date(31, Month.JANUARY, 2024)
        val range = Date(30, Month.JANUARY, 2024)..Date(31, Month.JANUARY, 2024)
        assertTrue(date in range)
    }
    @Test
    fun checkDateInRangeStartInclusive() {
        val date = Date(30, Month.JANUARY, 2024)
        val range = Date(30, Month.JANUARY, 2024)..Date(31, Month.JANUARY, 2024)
        assertTrue(date in range)
    }
    @Test
    fun checkDateInRangeInclusive() {
        val date = Date(15, Month.JANUARY, 2024)
        val range = Date(14, Month.JANUARY, 2024)..Date(16, Month.JANUARY, 2024)
        assertTrue(date in range)
    }
    @Test
    fun checkDateInRangeEndExclusive() {
        val date = Date(31, Month.JANUARY, 2024)
        val range = Date(30, Month.JANUARY, 2024)..< Date(31, Month.JANUARY, 2024)
        assertFalse(date in range)
    }
    @Test
    fun checkDateOutOfRangeBeforeInclusive() {
        val date = Date(15, Month.JANUARY, 2024)
        val range = Date(16, Month.JANUARY, 2024)..< Date(17, Month.JANUARY, 2024)
        assertFalse(date in range)
    }
    @Test
    fun checkDateOutOfRangeAfterInclusive() {
        val date = Date(18, Month.JANUARY, 2024)
        val range = Date(16, Month.JANUARY, 2024)..< Date(17, Month.JANUARY, 2024)
        assertFalse(date in range)
    }
}