package iam.thevoid.multiplatform.datetime.model

@kotlinx.serialization.Serializable
data class Time(val value: Int = 0) : Comparable<Time> {

    constructor(hours: Int = 0, minutes: Int = 0, seconds: Int = 0) :
        this(hours * SECONDS_IN_HOUR + minutes * SECONDS_IN_MINUTE + seconds)

    val hours: Int
        get() = value / SECONDS_IN_HOUR

    val minutes: Int
        get() = value % SECONDS_IN_HOUR / SECONDS_IN_MINUTE

    val seconds
        get() = value % SECONDS_IN_MINUTE

    override fun toString(): String {
        fun Int.comp() = toString().padStart(2, '0')
        return "${hours.comp()}:${minutes.comp()}:${seconds.comp()}"
    }

    override fun compareTo(other: Time): Int {
        return hours.compareTo(other.hours).takeIf { it != 0 }
            ?: minutes.compareTo(other.minutes).takeIf { it != 0 }
            ?: seconds.compareTo(other.seconds)
    }
    companion object {
        const val SECONDS_IN_MINUTE = 60
        const val SECONDS_IN_HOUR = 60 * SECONDS_IN_MINUTE
        val MIDNIGHT = Time(0)
    }
}
