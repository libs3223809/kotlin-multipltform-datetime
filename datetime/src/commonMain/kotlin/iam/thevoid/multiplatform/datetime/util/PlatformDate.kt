package iam.thevoid.multiplatform.datetime.util

expect class PlatformDate

expect val PlatformDate.timeInMillis: Long
