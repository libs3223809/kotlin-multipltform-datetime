package iam.thevoid.multiplatform.datetime.model

import iam.thevoid.multiplatform.datetime.util.DateUtils
import iam.thevoid.multiplatform.datetime.util.timeInMillis
import kotlin.time.Duration
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.minutes

@kotlinx.serialization.Serializable
data class Date(
    val day: Int,
    val month: Month,
    val year: Int
) : Comparable<Date> {

    val millis
        get() = DateUtils.toPlatformDate(this).timeInMillis

    val epochDay
        get() = millis.milliseconds.inWholeDays

    val daysInYear = if (DateUtils.isLeapYear(year)) DAYS_IN_LEAP_YEAR else DAYS_IN_YEAR

    val daysInMonth = daysInMonth(month)

    fun daysInMonth(month: Month) = month.daysCount(year)

    override fun compareTo(other: Date): Int {
        return year.compareTo(other.year).takeIf { it != 0 }
            ?: month.compareTo(other.month).takeIf { it != 0 }
            ?: day.compareTo(other.day)
    }

    operator fun minus(date: Date): Duration = DateUtils.between(this, date)

    // Current date 00:00:00
    fun toMinDateTime(): DateTime {
        return DateTime(this, Time.MIDNIGHT)
    }

    // Current date 23:59:59
    fun toMaxDateTime(): DateTime {
        return DateTime(this + 1.days, Time.MIDNIGHT) - 1.minutes
    }

    override fun toString(): String {
        return "${day.toString().padStart(2, '0')} ${month.short} $year"
    }
    companion object {

        private const val DAYS_IN_YEAR = 365
        private const val DAYS_IN_LEAP_YEAR = 365

        private const val SQLITE_DATE_PATTERN = "yyyy-MM-dd"

        fun fromSqliteDate(date: String): Date {
            return DateUtils.parseDate(SQLITE_DATE_PATTERN, date)
        }

        fun toSqliteDate(date: Date): String {
            return DateUtils.formatDate(SQLITE_DATE_PATTERN, date)
        }

        fun create(day: Int, month: Int, year: Int) : Date? {
            if (month !in Month.JANUARY.number..Month.DECEMBER.number) return null
            val eMonth = Month.byNumber(month)!!
            if (day !in 1..eMonth.daysCount(year)) return null
            return Date(day = day, month = eMonth, year = year)
        }
    }

    operator fun minus(duration: Duration) = this + -duration

    operator fun plus(duration: Duration) = DateUtils.plus(this, day = duration.inWholeDays.toInt())
}
