package iam.thevoid.multiplatform.datetime.model

import iam.thevoid.multiplatform.datetime.util.DateUtils

enum class Month(val short: String) {
    JANUARY("jan"),
    FEBRUARY("feb"),
    MARCH("mar"),
    APRIL("apr"),
    MAY("may"),
    JUNE("jun"),
    JULY("jul"),
    AUGUST("aug"),
    SEPTEMBER("sep"),
    OCTOBER("oct"),
    NOVEMBER("nov"),
    DECEMBER("dec");

    fun next() = if (ordinal + 1 >= Month.entries.size) JANUARY else entries[ordinal + 1]

    fun prev() = if (ordinal == 0) DECEMBER else entries[ordinal - 1]

    @Suppress("MagicNumber")
    fun daysCount(year: Int) = when (this) {
        JANUARY -> 31
        FEBRUARY -> if (DateUtils.isLeapYear(year)) 29 else 28
        MARCH -> 31
        APRIL -> 30
        MAY -> 31
        JUNE -> 30
        JULY -> 31
        AUGUST -> 31
        SEPTEMBER -> 30
        OCTOBER -> 31
        NOVEMBER -> 30
        DECEMBER -> 31
    }

    val number = ordinal + 1

    companion object {
        fun byNumber(number: Int) = if (number in 1..12) entries[number - 1] else null
    }
}
