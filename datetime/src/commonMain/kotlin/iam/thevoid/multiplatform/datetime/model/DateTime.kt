package iam.thevoid.multiplatform.datetime.model

import iam.thevoid.multiplatform.datetime.util.DateUtils
import kotlin.time.Duration
import kotlin.time.Duration.Companion.days

@kotlinx.serialization.Serializable
data class DateTime(val date: Date, val time: Time = Time()) : Comparable<DateTime> {
    companion object {

        private const val SQLITE_DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss"

        fun fromSqliteDate(date: String): DateTime {
            return DateUtils.parseDateTime(SQLITE_DATE_TIME_PATTERN, date)
        }

        fun toSqliteDate(date: DateTime): String {
            return DateUtils.formatDateTime(SQLITE_DATE_TIME_PATTERN, date)
        }
    }

    override fun toString(): String {
        return "$date $time"
    }

    override fun compareTo(other: DateTime): Int {
        return date.compareTo(other.date).takeIf { it != 0 } ?: time.compareTo(other.time)
    }

    operator fun minus(duration: Duration): DateTime = this + -duration

    operator fun minus(dateTime: DateTime): Duration {
        return DateUtils.between(this, dateTime)
    }

    operator fun plus(duration: Duration): DateTime {
        val seconds = duration.inWholeSeconds
        return if (seconds.toInt().toLong() == seconds) {
            DateUtils.plus(this, second = duration.inWholeSeconds.toInt())
        } else {
            val day = duration.inWholeDays
            val secondsRemainder = duration.inWholeSeconds % day.days.inWholeSeconds
            DateUtils.plus(this, day = day.toInt(), second = secondsRemainder.toInt())
        }
    }
}
