package iam.thevoid.multiplatform.datetime.util

import iam.thevoid.multiplatform.datetime.model.Date
import iam.thevoid.multiplatform.datetime.model.DateTime
import iam.thevoid.multiplatform.datetime.model.DayOfWeek
import iam.thevoid.multiplatform.datetime.model.Time
import kotlin.time.Duration

const val DAYS_IN_WEEK = 7

expect object DateUtils {

    fun toPlatformDate(date: Date): PlatformDate
    fun toPlatformDate(date: DateTime): PlatformDate
    fun platformDateToDateTime(date: PlatformDate): DateTime
    fun platformDateToDate(date: PlatformDate): Date
    fun platformDateToTime(date: PlatformDate): Time

    fun parseDate(format: String, date: String): Date
    fun parseTime(format: String, date: String): Time
    fun parseDateTime(format: String, date: String): DateTime

    fun formatDate(format: String, date: Date): String
    fun formatTime(format: String, time: Time): String
    fun formatDateTime(format: String, dateTime: DateTime): String

    fun createDate(): Date
    fun createTime(): Time

    fun isLeapYear(year: Int): Boolean
    fun isLeapYear(date: Date): Boolean

    fun overlappedDays(start: DateTime, end: DateTime): Int
    fun overlappedDays(start: Date, end: Date): Int

    fun isToday(date: Date): Boolean
    fun isToday(dateTime: DateTime): Boolean
    fun isLastDay(dateTime: DateTime): Boolean
    fun isThisWeek(dateTime: DateTime): Boolean
    fun isLastWeek(dateTime: DateTime): Boolean
    fun isThisMonth(dateTime: DateTime): Boolean
    fun isLastYear(dateTime: DateTime): Boolean
    fun isThisYear(dateTime: DateTime): Boolean

    fun dayOfWeek(date: Date): DayOfWeek

    internal fun between(start: DateTime, end: DateTime): Duration
    internal fun between(start: Date, end: Date): Duration
    internal fun plus(date: Date, day: Int): Date
    internal fun plus(dateTime: DateTime, day: Int = 0, second: Int = 0): DateTime
}

fun DateUtils.createDateTime() =
    DateTime(createDate(), createTime())
