plugins {
    kotlin(Dependencies.Plugin.multiplatform)
    kotlin(Dependencies.Plugin.kotlinSerialization)
    id(Dependencies.Plugin.Local.android)
    id(Dependencies.Plugin.Local.detekt)
    id("modules-test-dependencies")
}

android {
    namespace = "iam.thevoid.multiplatform.datetime"
}

group = "iam.thevoid.multiplatform.datetime"
version = 1.0

kotlin {
    androidTarget()
    jvm()
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(libs.kotlinx.serialization.json)
            }
        }
        val androidMain by getting
        val jvmMain by getting
    }
}