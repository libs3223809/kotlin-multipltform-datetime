import org.gradle.api.JavaVersion
import org.jetbrains.kotlin.gradle.dsl.JvmTarget

object Versions {

    val javaVersion: JavaVersion = JavaVersion.VERSION_11

    val jvmTarget = JvmTarget.JVM_11

    const val multiplatform = "1.8.0"

    object Android {
        const val minSdk = 24
        const val targetSdk = 34
        const val compileSdk = 35
    }
}