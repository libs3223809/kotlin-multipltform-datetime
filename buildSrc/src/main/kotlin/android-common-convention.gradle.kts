import com.android.build.gradle.BaseExtension

fun Project.android(configure: BaseExtension.() -> Unit): Unit =
    extensions.configure("android", configure)

android {
    compileSdkVersion(Versions.Android.compileSdk)
    defaultConfig {
        minSdk = Versions.Android.minSdk
        targetSdk = Versions.Android.targetSdk
    }
    compileOptions {
        sourceCompatibility = Versions.javaVersion
        targetCompatibility = Versions.javaVersion
    }

    buildTypes {
        BuildTypes.names.forEach(::maybeCreate)
    }
}

