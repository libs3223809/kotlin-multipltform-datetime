object Dependencies {

    object Plugin {
        const val multiplatform = "multiplatform"
        const val kotlinSerialization = "plugin.serialization"

        object Local {
            const val android = "android-library-convention"
            const val kotlin = "kotlin-convention"
            const val detekt = "detekt-convention"
        }
    }
}